'use strict'

var express = require('express')

var mongoose = require('mongoose'),
  Bill = mongoose.model('Bill')

var fs = require('fs')
var multiparty = require('multiparty');

var atob = require('atob');
//var cors = require('cors');

module.exports = function(Addbill, database) {
	return {
		uploadImg: function(req, res) {
      var count = 0;
      var form = new multiparty.Form();
      var filename;
      var savePath;
      console.log('form');
      form.on('error', function(err) {
        console.log('Error parsing form: ' + err.stack);
      });
      form.on('part', function(part) {
        if (!part.filename) {
          console.log('got field named ' + part.name);
          part.resume();
        }
        if (part.filename) {
          count++;
          console.log('got file named ' + part.name);
          part.resume();
        }
      });
      form.on('file', function(name, file) {
        //save file on server in newPath location
        filename = file.path.substr(file.path.length - 28); //zCwJVeNPQFhiptzm7e4GRJ1j.jpg
        var newPath = './packages/custom/addbill/public/uploads/' + filename;
        // image Path on server
        savePath = req.protocol+'://'+req.get('host') + '/addbill/uploads/' + filename;
        console.log("paths : " + filename, newPath, savePath);
        //originalFileName; // penguins.jpg
        //file.path; // C:\Users\Dell\AppData\Local\Temp\zCwJVeNPQFhiptzm7e4GRJ1j.jpg
        //imagePath = newPath; //./packages/custom/addbill/server/uploads/Penguins.jpg
        var data = fs.readFileSync(file.path, 'base64');
        var buffer = new Buffer(data, 'base64');
        fs.writeFileSync(newPath, atob(buffer), 'binary', function(err) {
          console.log("Function of err");
          if (err)
            console.log('ERR!' + err);
          else
            console.log('Written to new location !');
        });
        //imagePath = savePath;
      });
      form.on('close', function() {
        console.log('Upload completed!');
        console.log("filename " + filename);
        res.json({
          'filepath': savePath
        });
      });
      form.parse(req);
    },

		postBill: function(req, res) {
			var newBill = new Bill({
				userid: req.body.userid,
				created: req.body.created,
				billImage: req.body.billImage, 
        imageLoc: req.body.imageLoc,
				amount: req.body.amount,
				detail: req.body.detail,
				date: req.body.date,
				claimed: req.body.claimed
			})
			newBill.save(function(err) {
				if (err)
					console.log(err);
			});
			res.json({'new_bill': newBill});
		},

    getBills: function(req, res) {
      // get bill details from database and send to client
      // limit = 10 ?
      console.log("user id: " + req.body.userid);
      Bill.find({
        userid: req.body.userid,
        claimed: false
      }).sort({
        date: 'asc'
      }).exec(function(err, results) {
        if (err)
          console.log('ERR! ', err);
        if (results != null) {
          console.log("get bills server: " + results);
          res.json({
            'getbills': results,
            'served': 'yes'
          });
        }
        if (results == null) {
          console.log("No bills to serve");
          res.json({
            'served': 'no'
          });
        }
      });
    },

    claimBills: function(req, res) {
      //console.log('got in');
      console.log(req.body.fromDate);
      Bill.find({
        userid: req.body.userid,
        claimed: false,
        date: {
          $gte: req.body.fromDate,
          $lte: req.body.toDate
        }
      }).sort({
        date: 'desc'
      }).exec(function(err, results) {
        if (err)
          console.log('ERR!' + err);
        //console.log(res.header('Access-Control-Allow-Origin'));
        res.json({
          'unclaimedBills': results
        });
      });
    },

    deleteBill: function(req) {
      console.log("Delete Bill");
      Bill.remove({
        "userid" : req.body.userid,
        "detail" : req.body.detail,
        "date": req.body.date
      }).exec(function(err, results) {
        if (err)
          console.log('Error deleting bill! ' + err);
        else
          console.log('Deleted Bill ' + req.body.detail);
      });
    }
  }
}
