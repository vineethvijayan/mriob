'use strict'

var mongoose = require('mongoose'), 
	Schema = mongoose.Schema;

var BillSchema = new Schema({
	userid: {
		type: String,
		required: true
	},
	created: {
		type: Date,
	},
	billImage: {
		type: String, // store path of image in server
	},
	imageLoc: {
		type: String,
	},
	amount: {
		type: Number,
		required: true
	},
	detail: {
		type: String,
		required: true
	},
	date: {
		type: Date,
		required: true
	},
	claimed: {
		type: Boolean,
		default: false
	}
});

var Bill = mongoose.model('Bill', BillSchema)