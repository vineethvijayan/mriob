'use strict';

/* jshint -W098 */
angular.module('mean.submit').controller('SubmitController', ['$rootScope','$scope', 'Global', 'Submit', '$state', '$http',
  function($rootScope, $scope, Global, Submit, $state, $http) {
    $http({
      method: 'GET',
      url: '/api/users/me'
    }).then(function successCallback(response) {
        $rootScope.userid = response.data.email;
        $rootScope.name = response.data.name;
        console.log($rootScope.userid);
        if($rootScope.userid == undefined) {
          console.log("Reroute");
          $state.go('auth');
        }
        if($rootScope.userid.substr($rootScope.userid.length - 12) != 'moonraft.com' ) {
          console.log("Not moonraft id");
          $state.go('auth');
        }
      }, function errorCallback(response) {
            console.log(response);
      });
    
    $scope.global = Global;
    $scope.package = {
      name: 'submit'
    };
    $scope.dateOptions = {

    };
    //$scope.formats = ['dd-MMMM-yy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = 'mediumDate';
    $scope.open = function($event) {
      $scope.status.opened = true;
    };
    $scope.status = {
      opened: false
    };
    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1
    };

    $scope.go = function() {
      $state.go('landing.searched', {
        fromDate: $scope.fromDate,
        toDate: $scope.toDate
      });
    }

  }
]);


angular.module('mean.submit').controller('BillsController', ['$rootScope','$scope', 'Global', 'Submit', '$http', '$stateParams', '$location',
  function($rootScope, $scope, Global, Submit, $http, $stateParams, $location) {

    console.log('userid ' + $rootScope.userid);
    var claimFilter = {
        userid:  $rootScope.userid,
        fromDate: $stateParams.fromDate,
        toDate: $stateParams.toDate
      }; 
    console.log("filter " + $stateParams.fromDate, $stateParams.toDate,  $rootScope.userid);
    $http.post('/api/addbill/claim', claimFilter).success(function(data) {
      $scope.bills = data.unclaimedBills;
    }).error(function(err) {
      console.log(err)
    });

    $scope.myData = [];

    $scope.toggleDisplay = function(index) {
      if ($scope.bills[index].show) {
        $scope.bills[index].show = false;
        return;
      }
      for (var i = 0; i < $scope.bills.length; i++) {
        $scope.bills[i].show = false;
      }
      $scope.bills[index].show = true;
    };

    $scope.getTotal = function(){
        var total = 0;
        for(var i = 0; i < $scope.bills.length; i++){
             var bill = $scope.bills[i];
             total += bill.amount;
          }
        console.log("Total amount: " + total)
        return total;
      };

    $scope.generateClaim = function() {
      $scope.total = $scope.getTotal();
      $http({
        method: 'POST',
        url: '/api/submit/generateClaim',
        data: { bills:$scope.bills, total:$scope.total, username: $rootScope.name, email: $rootScope.userid },
        responseType: 'blob'
      }).success(function(blob) {
        //blob.type = "application/pdf";
        var url = window.URL.createObjectURL(blob);
        $scope.downloadFile = url;
      });
    }
  }
]);
