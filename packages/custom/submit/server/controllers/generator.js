'use strict';

var conversion = require("phantom-html-to-pdf")();


module.exports = function(app) {
  return {
    generateReport: function(Submit, app, req, res) {
      //var bills = req.body.bills;
      Submit.render('bills', {
        package: 'submit',
        bills: req.body.bills,
        username: req.body.username,
        email: req.body.email,
        total: req.body.total
      }, function(err, html) {
        conversion({
          html: html
        }, function(err, pdf) {
          if (err) {
            res.status(500).send(err);
          }
          try {
            res.setHeader('Content-Type','application/pdf');
            pdf.stream.pipe(res);
          } catch (err) {
            res.status(500).send(err);
          }

        })
      });
    }
  }
};
