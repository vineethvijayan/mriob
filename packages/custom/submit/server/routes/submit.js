'use strict';



/* jshint -W098 */
// The Package is past automatically as first parameter
module.exports = function(Submit, app, auth, database) {
  var generator = require('../controllers/generator')(app);

  app.post('/api/submit/generateClaim', function(req, res, next) {
    generator.generateReport(Submit, app, req, res);
  });

};
